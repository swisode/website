_Benenne Dein Ticket wie folgt: "Freier Dienst: **Name des Dienstes**"_

**Textvorschlag**

_Ein Absatz mit zwei-drei Sätzen mit folgendem Inhalt:_
- _Worum handelt es sich bei dem Dienst_
- _Was zeichnet den Dienst aus (auch gegenüber unfreien Diensten)_
- _Falls Du Vorschläge für diese Vorlage hast bitte in #3 kommentieren_

_Ein zweiter Absatz mit wissenswerten Details, die nicht in den ersten Absatz passen._

**Registrierung:** _Nur nutzen falls eine Registrierung erforderlich ist_
- _[Erste.de](https://switchingsocial.de)_
- _[ZweiterServer.de](https://switchingsocial.de)_
- _[Liste weiterer Server, möglichst deutschsprachig](https://switchingsocial.de)_
- _[Selbsthosting wenn unkompliziert](https://switchingsocial.de)_

**Entdecken:** 
- _[Spannende Links, die sonst nirgends passen](https://switchingsocial.de)_

**Android-Apps:**
- _[Einsteigerfreundliche App](https://switchingsocial.de)_
- _[Weitere App](https://switchingsocial.de)_
- _Bitte möglichst die App-Webseite verlinken, nicht den Playstore-Eintrag. Besonders falls es eine FDroid-Version gibt sollte es möglichst eine Webseite zur App sein, die alle Download-Möglichkeiten verlinkt._

**iOS-Apps:**
- _[Einsteigerfreundliche App](https://switchingsocial.de)_
- _[Weitere App](https://switchingsocial.de)_
- _Bitte möglichst die App-Webseite verlinken, nicht den Appstore-Eintrag._

**Unterstützen:**
- _Nur falls das Unterstützen in sehr einfacher Form passieren kann (Spenden, Fragen beantworten, ...)_