## Über

**SwitchingSocial.de** ist eine Sammlung an Hilfestellungen, um einfach zu freier Software zu wechseln.

Diese Seite ist nur möglich dank zahlreicher freier Projekte von denen hier die wichtigsten benannt seien:

- [switching.social](https://switching.social): Die Seite, mit der alles begann :-)
- [Grav](https://getgrav.org/): Ein dateibasiertes und flexibles Content-Management-System
- [GitLab](http://gitlab.com/): Eine Quelltext-Versionierungsplattform mit vielen zusätzlichen Werkzeugen

## Mitarbeit an der Webseite

Falls Du Interesse hast an dieser Webseite mitzuwirken, so schau gerne in unserem 
[Ticket Nummer 1](https://gitlab.com/swisode/website/issues/1) vorbei für einen
kurzen Überblick über das Projekt und darüber wie Du Dich einbringen kannst.

An dieser Stelle sei ausdrücklich allen gedankt, die dies bereits getan haben.

## Lizenz

[Switchingsocial.de](https://switchingsocial.de) ist im wesentlichen eine 
deutsche Übersetzung der englischsprachigen Seite [switching.social](https://switching.social).
Ferner wurden einige Anpassungen bei Auswahl und Formulierung der Inhalte vorgenommen,
damit sich die Inhalte besser für deutschsprachige Nutzer eignen.

Alle Inhalte sind entsprechend des englischen Originals unter der freien Lizenz 
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) veröffentlicht.

## Lokale Installation

Folgende Schritte sind zu tun, um eine lokale Kopie dieser Seite zu verwenden:

1. Lade Grav [von der Downloadseite](http://getgrav.org/downloads) ohne Admin-Plugin herunter.
2. Extrahiere das ZIP-Archiv in ein lokales Verzeichnis (z.B. `~/www/brick.camp/`)
3. Wechsle in den `user`-Unterordner (z.B. `~/www/brick.camp/user/`) und lösche alle enthaltenen Inhalte.
4. Nun lade den Inhalt von SwitchingSocial.de herunter. Führe dafür in diesem Ordner den folgenden Befehl aus:
`git clone https://gitlab.com/swisode/website .`
5. Wechsle zurück ins Elternverzeichnis (z.B. `~/www/brick.camp/` ) und führe folgenden Befehl aus:
`bin/grav install`
Hierbei werden keine Änderungen an deinem System vorgenommen, sondern lediglich weitere Dateien nachgeladen.
6. Starte den mitgelieferten Webserver, indem Du Anfragen an eine lokale Adresse auf die `router.php` leitest. Das tust du z.B. wie folgt:
`php -S 127.0.0.1:8080 system/router.php`
7. Beim Aufruf der Adresse im lokalen Browser solltest Du die Webseite sehen.