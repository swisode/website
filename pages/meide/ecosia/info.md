---
title: Ecosia
icon: icon.png

taxonomy:
    replaces: [google-search]
---
*Ecosia* fördert den Naturschutz, aber leitet [Suchanfragen samt IP-Adresse an Microsoft](https://info.ecosia.org/privacy#privacy-faq-item-5).

===

**Ecosia** ist eine ökologische Suchmaschine, welche mit ihren Einnahmen Naturschutz-Organisationen beim Pflanzen von Bäumen unterstützt. Die Suchergebnisse und Werbeanzeigen werden von Bing bezogen, weshalb [IP-Adresse und Suchbegriffe an Microsoft](https://info.ecosia.org/privacy#privacy-faq-item-6) gehen.

! **Webseite:** [Ecosia](https://www.ecosia.org/)