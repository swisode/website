---
title: Mojeek
icon: icon.png

taxonomy:
    replaces: [google-search]
---
*Mojeek* hat derzeit keine deutschsprachige Datenschutzerklärung.

===

ist eine datenschutzfreundliche Suchmaschine mit Sitz in Großbritannien. Sie erstellt einen eigenen Suchindex. 

Hinweis: Aktuell fehlt noch eine deutschsprachige Version der Datenschutzerklärung.

! **Webseite:** [Mojeek](https://mojeek.de)