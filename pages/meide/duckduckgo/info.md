---
title: DuckDuckGo
icon: icon.svg

taxonomy:
    replaces: [google-search]
---
*DuckDuckGo* ist sehr populär, fällt als amerikanisches Unternehmen jedoch unter den [Patriot Act](https://de.wikipedia.org/wiki/USA_PATRIOT_Act#Auswirkungen_auf_den_Schutz_personenbezogener_Daten_und_geistigen_Eigentums) und hat keine deutschsprachige Datenschutzerklärung.

===

**DuckDuckGo** ist die populärste datenschutzfreundliche Suchmaschine und hat ihren Sitz in den USA. Sie finanziert sich über Werbung, jedoch ohne Trackingmethoden.

Als amerikanisches Unternehmen fällt sie unter den [Patriot Act](https://de.wikipedia.org/wiki/USA_PATRIOT_Act) und hat keine deutschsprachige Datenschutzerklärung. Deshalb ist diese Seite nicht öffentlich.

! **Webseite:** [DuckDuckGo](https://duckduckgo.com)