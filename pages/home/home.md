---
title: SwitchingSocial
subtitle: Ethische, unkomplizierte und datenschutz-freundliche Alternativen zu bekannter Software

content:
    items: 
        - '@page.children': /ersetze
        - '@page.self': /ueber
        - '@page.self': /ueber/wechsel
    order:
        by: default
        custom:
            - facebook
            - whatsapp
            - twitter
            - instagram
            - youtube
            - google-maps

body_classes: title-center title-h1h2
hero_classes: hero-tiny title-h1h2
---