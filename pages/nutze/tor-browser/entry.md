---
title: Tor Browser
icon: icon.svg

taxonomy:
    replaces: [google-chrome]
    tag: [f-droid]
---
**Tor Browser** ist eine speziell modifizierte Version von Firefox. Sie ist etwas schwieriger zu verwenden als Firefox selbst, schützt Deine Privatsphäre jedoch deutlich besser. Der Tor Browser macht es sich zur Aufgabe, die Menge an identifizierenden Informationen zu minimieren, die Du beim Besuch von Webseiten preisgibst. 

Dafür verwendet er das **Tor-Netzwerk**: Ein Verbund von Rechnern, die von freiwilligen Helfern betrieben werden und Deine Anfragen durch mehrfaches Umleiten anonymisieren. Dies macht es für Unternehmen deutlich schwieriger, Dich auszuspionieren. Es verlangsamt aber auch das Surfen, da Du nicht direkt mit den Webseiten verbunden wirst.

! **Webseite:** [Tor-Projekt](https://www.torproject.org/de/)
! **Download:** [Desktop](https://www.torproject.org/de/download/) | [Android](https://www.torproject.org/de/download/#android) | [iOS](https://apps.apple.com/us/app/onion-browser/id519296448)

===