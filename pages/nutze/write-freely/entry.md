---
title: Write Freely
icon: icon.svg

taxonomy:
    replaces: [medium]
---
**Write Freely** ist ein [freier](/nutze/freie-software) Blogging-Dienst, der klar den Fokus auf den Text legt - ablenkungsfrei und mit sauberer, übersichtlicher Ästhetik.

Die Seite sammelt nur ein absolutes Minimum an notwendigen Informationen und ist damit sehr datenschutz-freundlich. Sie ist ferner Teil des [Fediverse](/nutze/fediverse), sodass Du darüber auch Interessierte auf anderen Plattformen erreichen kannst.

! **Webseite:** [writefreely.org](https://writefreely.org/)
! **Registrieren:** [write.as](https://write.as/) | [Qua.name](https://qua.name/) | [weitere](https://writefreely.org/instances) | [selbst betreiben](https://writefreely.host/)
! **Android-Apps:** [Write.as](https://play.google.com/store/apps/details?id=com.abunchtell.writeas&noprocess)
! **iOS-Apps:** [Write.as](https://apps.apple.com/de/app/write-as/id1000755153)
===