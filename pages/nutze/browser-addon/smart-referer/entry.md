---
title: Smart Referer
icon: icon.svg

taxonomy:
    extends: [firefox]
---
**Smart Referer** sorgt dafür, dass "Referer" nicht website-übergreifend mitgeteilt werden. Dabei handelt es sich um die technische Angabe, über welche Webseite Du die aktuelle Datei oder Seite erreicht hast. 

! **Download:** [Firefox](https://addons.mozilla.org/de/firefox/addon/smart-referer/)
! **Entdecken:** [Artikel zu Smart Referer](https://www.kuketz-blog.de/firefox-smart-referer-firefox-kompendium-teil6/)

===