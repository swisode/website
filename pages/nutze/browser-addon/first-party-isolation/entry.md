---
title: First Party Isolation
icon: icon.svg

taxonomy:
    extends: [firefox]
---
**First Party Isolation** sorgt dafür, dass alle Daten, die eine Website in Deinem Browser ablegt, auch nur von dieser wieder ausgelesen werden können. Damit wird verhindert, dass Drittanbieter Dich über verschiedene Websites hinweg verfolgen. 

Die Erweiterung kann derzeit noch bei (wenigen) Logins zu Problemen führen. Im Fall der Fälle kannst Du sie einfach per Klick auf das Icon in der Symbolleiste kurzzeitig deaktivieren.

! **Download:** [Firefox](https://addons.mozilla.org/de/firefox/addon/first-party-isolation/)
! **Entdecken:** [Artikel zu First Party Isolation](https://www.kuketz-blog.de/firefox-first-party-isolation-firefox-kompendium-teil4/)

===