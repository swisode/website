---
title: uBlock Origin
icon: icon.svg

taxonomy:
    extends: [firefox]
---
**uBlock Origin** ist eine [freie](/nutze/freie-software) Browser-Erweiterung, die ungewollte Inhalte von Webseiten blockiert. Dies umfasst neben Werbung standardmäßig auch Tracker und Schadsoftware. 

Durch das Abonnieren zusätzlicher Filterlisten können auch andere Bestandteile von Webseiten entfernt werden. Damit uBlock Origin optimal funktioniert sollten keine anderen Adblocker-Erweiterungen installiert sein. Andernfalls könnten sich die Erweiterungen gegenseitig behindern.

! **Download:** [Firefox](https://addons.mozilla.org/de/firefox/addon/ublock-origin/) | [Chrome](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm) | [Edge](https://www.microsoft.com/de-de/p/ublock-origin/9nblggh444l4?rtc=1&activetab=pivot:overviewtab) | [Opera](https://addons.opera.com/de/extensions/details/ublock/)
! **Entdecken:** [Artikel zu uBlock Origin](https://www.kuketz-blog.de/firefox-ublock-origin-firefox-kompendium-teil2/)

===