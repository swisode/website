---
title: Decentraleyes
icon: icon.png

taxonomy:
    extends: [firefox]
---
**Decentraleyes** ersetzt Javascript-Dateien, die über Drittanbieter eingebunden werden, durch lokal gespeicherte. So werden Anfragen bei proprietären Diensten vermieden, ohne die Webseite in ihrer Funktionalität einzuschränken. Diese Erweiterung ergänzt Filter-Erweiterungen wie uBlock Origin.

! **Download:** [Firefox](https://addons.mozilla.org/de/firefox/addon/decentraleyes/) | [Chrome](https://chrome.google.com/webstore/detail/decentraleyes/ldpochfccmkkmhdbclfhpagapcfdljkj) | [Opera](https://addons.opera.com/de/extensions/details/decentraleyes/)
! **Entdecken:** [Artikel zu Decentraleyes](https://www.kuketz-blog.de/firefox-decentraleyes-firefox-kompendium-teil3/)

===