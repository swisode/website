---
title: Peertube

icon: icon.svg

taxonomy:
    replaces: [youtube]
    tag: [fediverse, f-droid]
---
**Peertube** ist eine Plattform zum Videoaustausch, die Dich nicht trackt und werbefrei ist. Die Anzahl der Inhalte ist derzeit noch überschaubar, jedoch wächst die Plattform sehr schnell. 

Im Gegensatz zu Youtube ist Peertube nicht zentralisiert. Stattdessen gibt es viele kleine Server, die ein gemeinsames Protokoll sprechen. So macht es keinen Unterschied, auf welchem Server Du Dich registrierst. Selbst über andere Plattformen des [Fediverse](/nutze/fediverse) kannst Du mit Peertube-Videos interagieren.

! **Registrierung:** [pe.ertu.be](https://pe.ertu.be/de-DE) | [video.tedomum.net](https://video.tedomum.net/de-DE) | [share.tube](https://share.tube/de-DE) | [weitere Instanzen](https://joinpeertube.org/de/#register) 
! **Entdecken:** [Webseite](https://joinpeertube.org/de/)
! **Android-Apps:** [Fedilab](https://fedilab.app/) | [Thorium](https://github.com/sschueller/peertube-android#download)

===