---
title: LibreOffice

icon: icon.svg
body_classes: title-center title-h1

taxonomy:
    replaces: [ms-office]
    tag: [f-droid]
---

**LibreOffice** ist eine [freie](/nutze/freie-software) und quelloffene Zusammenstellung von Bürosoftware. Sie ist einfach zu benutzen und durchgängig kompatibel zu Microsoft-Office-Dokumenten.

Teil der Pakets sind Programme zur Textverarbeitung (_Writer_), Tabellenkalkulation (_Calc_), Präsentation (_Impress_) und zur Erstellung von Zeichnungen (_Draw_). Auch Programme zur Bearbeitung von Datenbanken (_Base_) und Formeln (_Math_) sind enthalten.

**Tipp:** Falls Dir die Programmoberfläche auf den ersten Blick nicht zusagt, findest Du im Menü unter _Ansicht > Benutzeroberfläche_ schickere Alternativen.

! **Webseite:** [LibreOffice](https://www.libreoffice.org/)
! **Downloads:** [Windows](https://www.libreoffice.org/download/download/?type=win-x86_64&lang=de&noprocess) | [macOS](https://www.libreoffice.org/download/download/?type=mac-x86_64&lang=de&noprocess) | [Linux](https://www.libreoffice.org/download/download/?type=deb-x86_64&lang=de&noprocess)
! **Android-Apps:** Viewer ([Google Play](https://play.google.com/store/apps/details?id=org.documentfoundation.libreoffice&hl=de&noprocess) / [FDroid](https://f-droid.org/packages/org.documentfoundation.libreoffice/)) | Impress Remote ([Google Play](https://play.google.com/store/apps/details?id=org.libreoffice.impressremote&noprocess) / [FDroid](https://f-droid.org/packages/org.libreoffice.impressremote/))
! **iPhone-Apps:** [Impress Remote](https://itunes.apple.com/us/app/libreoffice-remote-for-impress/id806879890)

===