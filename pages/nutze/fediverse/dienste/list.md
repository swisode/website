---
title: Fediverse
subtitle: Föderierte Dienste

content:
    items: 
        - '@taxonomy.tag': fediverse
    order:
        by: title

hero_classes: hero-tiny title-h1h2
---
Hier findest Du eine alphabetisch sortierte Liste der Dienste, die Teil des [Fediverse](/nutze/das-fediverse) sind. Diese Liste ist nicht vollständig und befindet sich noch in Arbeit.