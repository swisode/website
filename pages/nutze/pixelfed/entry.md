---
title: Pixelfed

icon: icon.svg

taxonomy:
    replaces: [instagram]
    tag: [fediverse]
---
**Pixelfed** ist der Name einer [freien](/nutze/freie-software) Software und des gleichnamigen, schnell wachsenden Netzwerks. Es ist auf das Teilen von Fotos ausgerichtet und orientiert sich in der Bedienung an Instagram.

Pixelfed ist dezentral organisiert und Teil des [Fediverse](/nutze/fediverse). Das bedeutet: Wie bei Deiner E-Mail-Adresse hast Du die freie Wahl des "Anbieters" (der Instanz). Egal welche Instanz Du wählst, über *@nutzername@instanzname* kannst Du jede beliebige andere Person erreichen - und sie Dich ebenfalls. Du kannst damit auch Inhalte mit Benutzern anderer Dienste wie z.B. Mastodon teilen.

! **Registrierung:** [pixelfed.de](https://pixelfed.de) | [selbst betreiben](https://app.spacebear.ee/)
! **Entdecken:** [Beispiel-Account](https://pixelfed.social/earth)
===