---
title: Firefox-Erweiterungen
subtitle: Empfohlene Add-Ons
content:
    items: 
        - '@taxonomy.extends': firefox
    order:
        by: default
        custom:
            - ublock-origin
            - decentraleyes
            - first-party-isolation
            - smart-referer
hero_classes: hero-tiny title-h1h2
---