---
title: Firefox
icon: icon.svg

taxonomy:
    replaces: [google-chrome]
    tag: [f-droid]
---
**Firefox** ist der populärste [freie](/nutze/freie-software) Webbrowser. Er ist nicht-kommerziell und wird von der gemeinnützigen Mozilla Foundation und Freiwilligen weiterentwickelt. 

Dank einer Vielzahl an Add-Ons ist der Browser beliebig erweiterbar. Einige ausgewählte [Erweiterungen für Deine Privatsphäre](/nutze/firefox/addons) sind besonders zu empfehlen.

! **Webseite:** [Mozilla Firefox](https://www.mozilla.org/de/firefox/)
! **Android-Apps:** [Firefox](https://play.google.com/store/apps/details?id=org.mozilla.firefox&noprocess) ([FDroid](https://f-droid.org/de/packages/org.mozilla.fennec_fdroid/)) | [Firefox Klar](https://play.google.com/store/apps/details?id=org.mozilla.klar&noprocess) ([FDroid](https://f-droid.org/de/packages/org.mozilla.klar/))
! **iOS-Apps:** [Firefox](https://apps.apple.com/de/app/internet-browser-firefox/id989804926) | [Firefox Klar](https://apps.apple.com/de/app/firefox-klar/id1073435754)
! **Entdecken:** [Empfohlene Add-Ons](/nutze/firefox/addons)
===