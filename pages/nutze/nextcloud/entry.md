---
title: Nextcloud
icon: icon.svg

taxonomy:
    replaces: [google-drive]
---
**Nextcloud** ist ein [freier](/nutze/freie-software) Dienst, mit dem sich Dateien online speichern, bearbeiten und mit anderen teilen lassen. Dank [vieler Erweiterungen](https://apps.nextcloud.com/) sind auch Videotelefonie, soziales Netzwerken, Kalendersynchronisation und vieles mehr möglich.

Als freie Software kann Nextcloud beliebig von Privatpersonen oder Firmen betrieben werden. Entsprechend stehen Dir viele Seiten zur Auswahl, bei denen Du Dich registrieren kannst. Oft sind Logo und Oberfläche an den Anbieter angepasst - dennoch ist die zugrunde liegende Software bei allen unten verlinkten Seiten NextCloud.

Viele Seiten bieten Dir einen beschränktem Speicherplatz gratis an und erheben erst für zusätzlichen Speicher Kosten.

! **Webseite:** [Nextcloud](https://framapic.org)
! **Registrieren:** [Nextcloud Registrierung](https://nextcloud.com/signup/) | [Nextcloud Anbieterliste](https://nextcloud.com/providers/)
! **Android-Apps:** [Nextcloud](https://play.google.com/store/apps/details?id=com.nextcloud.client&noprocess)
! **iOS-App:** [Nextcloud](https://itunes.apple.com/de/app/nextcloud/id1125420102)
===