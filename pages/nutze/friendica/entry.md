---
title: Friendica

icon: icon.svg

taxonomy:
    replaces: [facebook, twitter]
    tag: [fediverse, f-droid]
---
**Friendica** ist ein [freies](/nutze/freie-software) und dezentrales soziales Netzwerk, dessen Funktionen an Facebook angelehnt sind. Es bietet neben den Kommunikationsmöglichkeiten Funktionen wie Events/Kalender, Fotoalben und Gruppen.

Als dezentrales Netzwerk besteht es aus vielen kleineren Seiten (Instanzen genannt), die untereinander verbunden sind. Du kannst ähnlich Deiner Mailadresse aus einer Vielzahl von Anbietern auswählen.

Friendica ist Teil des [Fediverse](../fediverse). Somit erreichst Du dort auch Leute auf Mastodon und Co. über die Kennung *@nutzername@instanzname*.

! **Registrierung:** [Nerdica.net](https://nerdica.net/) | [Libranet.de](https://libranet.de/) | [mehr deutschsprachige](https://the-federation.info/friendica#nodes-table)
! **Android-Apps:** [Fedilab](https://fedilab.app/) | [DiCa](https://play.google.com/store/apps/details?id=cool.mixi.dica&noprocess)

===