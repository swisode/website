---
title: Mailbox.org

icon: icon.svg
body_classes: title-center title-h1

taxonomy:
    replaces: gmail
    warning: proprietary
---

**Mailbox.org** ist ein Berliner E-Mail-Anbieter mit Fokus auf Privatsphäre und Datensicherheit. Es bietet Dir neben einem Postfach auch Kalender und Adressbuch, welche Du mit Deinen Geräten synchronisieren kannst. Die Kosten liegen bei 1,- € pro Monat.

Optional kannst Du Dein Postfach per Zwei-Faktor-Authentifizierung schützen. Auch eine eigene Domain kann auf Wunsch eingerichtet werden.

! **Webseite:** [Mailbox.org](https://mailbox.org/de/)

===