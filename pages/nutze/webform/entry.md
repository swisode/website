---
title: Webform
icon: icon.png

taxonomy:
    replaces: [google-forms]
---
**Webform** ist eine quelloffene Umfrage-Software. Sie lässt sich selbst betreiben oder über Webform.com nutzen. Letzteres ist gratis und in einer Bezahlversion möglich.

! **Webseite:** [Webform.com](http://webform.com/)
===