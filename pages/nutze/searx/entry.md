---
title: Searx
icon: icon.svg

taxonomy:
    replaces: [google-search]
---
**Searx** ist eine [freie](/nutze/freie-software) Meta-Suchmaschine. Sie bündelt die Suchergebnisse vieler populärer Suchmaschinen, ohne dass diese die Anfragenden identifizieren können.

Searx lässt sich selbst betreiben - entsprechend groß ist die Auswahl an Seiten.

! **Webseiten:** [Honigdachse.de](https://suche.honigdachse.de/) | [searx.me](https://searx.me/) | [Gruble.de](https://www.gruble.de/) | [weitere](https://github.com/asciimoo/searx/wiki/Searx-instances#alive-and-running)

===