---
title: Startpage
icon: icon.png
type: nonfree

taxonomy:
    replaces: [google-search]
---
**Startpage** ist eine niederländische Suchmaschine, die Deine Suchanfragen anonym an Google weiterleitet. So bekommst Du eine hohe Qualität bei den Suchergebnissen und Deine Privatsphäre bleibt gewahrt. 

Ferner findet keine Personalisierung statt: Zu gleichen Suchanfragen sehen alle die gleiche Trefferliste. Das verringert die Gefahr von Filterblasen. Leider führen Dich die Anzeigen weiterhin über Google ans Ziel, weshalb Du diese meiden oder per [uBlock Origin](/nutze/firefox/addons/#ublock-origin) filtern solltest.

! **Webseite:** [Startpage.com](https://startpage.com)
===