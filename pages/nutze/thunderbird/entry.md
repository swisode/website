---
title: Thunderbird
icon: icon.png

taxonomy:
    replaces: [ms-office]
---
**Thunderbird** ist die populärste [freie](/nutze/freie-software) E-Mail-Anwendung für Computer. Ähnlich zu Firefox lässt es sich einfach durch eine Vielzahl an Erweiterungen und Themes an Deine Wünsche anpassen.

! **Webseite:** [Mozilla Thunderbird](https://www.thunderbird.net/de/) 
! **Downloads:** [Desktop](https://www.thunderbird.net/de/thunderbird/all/#de)
===