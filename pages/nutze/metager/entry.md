---
title: MetaGer
icon: icon.png
type: free

taxonomy:
    replaces: [google-search]
---
**MetaGer** ist eine [freie](/nutze/freie-software) Meta-Suchmaschine. Sie bündelt die Suchergebnisse vieler Suchmaschinen, ohne dass diese den Anfragenden identifizieren können. Das Projekt wird vom Verein Suma e.V. gemeinsam mit der Uni Hannover betrieben und entwickelt. 

! **Webseite:** [MetaGer.de](https://metaGer.de)

===