---
title: OnlyOffice
icon: icon.svg

taxonomy:
    replaces: [ms-office]
---
**OnlyOffice** ist ein [freies](/nutze/freie-software) Bürosoftware-Paket - von Dokumentbearbeitung über Emails, Datenbanken, Projektmanagement, Kalender, Foren und mehr.

Es kann als Online-Service gegen eine monatliche Gebühr genutzt werden - und ist kostenlos für gemeinnützige Organisationen. Mit etwas technischem Know-How lässt sich der Dienst auch selbst online betreiben.

Alternativ gibt es eine Offline-Version, die sich einfach und kostenlos auf dem eigenen Rechner nutzen lässt. Optional kann jede Version bequem mit NextCloud zusammenarbeiten.

! **Webseite:** [OnlyOffice](https://www.onlyoffice.com/de/)
! **Downloads:** [Desktop](https://www.onlyoffice.com/de/download-desktop.aspx) | [Android](https://play.google.com/store/apps/details?id=com.onlyoffice.documents&noprocess) | [iOS](https://itunes.apple.com/de/app/onlyoffice-documents/id944896972)
! **Registrieren:** [OnlyOffice Cloud](https://www.onlyoffice.com/de/registration.aspx) | [Als Non-Profit](https://www.onlyoffice.com/de/nonprofit-organizations.aspx)
===