---
title: Framapic (Lutim)
icon: icon.png

taxonomy:
    replaces: [imgur, photobucket]
	tag: [framasoft]
---
**Framapic** ist ein datenschutz-freundlicher [freier](/nutze/freie-software) Dienst zum Teilen von Fotos. Er wird von der gemeinnützigen Organisation [Framasoft](https://framasoft.org) entwickelt und betrieben. Das Programm basiert auf **Lutim**, welches ebenfalls genutzt werden kann.

! **Webseite:** [Framapic.org](https://framapic.org) | [Picat.drycat.fr](https://picat.drycat.fr/)
===