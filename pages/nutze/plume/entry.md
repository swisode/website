---
title: Plume
icon: icon.svg

taxonomy:
    replaces: [medium]
---
**Plume** ist eine [freie](/nutze/freie-software) Blogging-Software, die wie [Write Freely](#write-freely) in das [Fediverse](/nutze/das-fediverse) integriert ist. So können zum Beispiel Nutzer von [Mastodon](/nutze/mastodon) Deinem Blog folgen.

! **Webseite:** [JoinPlu.me](https://joinplu.me/)
! **Registrieren:** [fediverse.blog](https://fediverse.blog/) | [plume.mastodon.host](https://plume.mastodon.host/) | [cafe.sunbeam.city](https://cafe.sunbeam.city/) | [weitere](https://joinplu.me/)
===