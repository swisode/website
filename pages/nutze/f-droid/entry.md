---
title: F-Droid
icon: icon.svg

taxonomy:
    replaces: [google-play]
---
**F-Droid** ist ein [freier](/nutze/freie-software) App-Store für Android-Geräte. Er verfolgt Deine Aktivitäten nicht und enthält ausschließlich Open-Source-Anwendungen. Bei diesen ist die Gefahr von Privatsphäre-Verletzungen deutlich geringer.

Die Installation von F-Droid ist etwas kompliziert, muss aber nur ein einziges Mal durchlaufen werden. Eine Installations-Anleitung ist unten verlinkt. Falls Dir einzelne Schritte Unbehagen bereiten, bitte eine technisch versierte Person in Deinem Bekanntenkreis um Hilfe.

Einmal installiert ist F-Droid ebenso einfach nutzbar wie jeder anderer App-Store. Du kannst Anwendungen einfach installieren und wie jede andere App über ihr Icon starten.

Viele Anwendungen sind sowohl in Google Play als auch in F-Droid verfügbar. Installiere solche lieber über F-Droid, das ist besser für Deine Privatsphäre. 

! **Webseite:** [F-Droid.org](https://f-droid.org/de/)
! **Entdecken:** [Anleitung zur Installation](https://mobilsicher.de/schritt-fuer-schritt/so-installieren-sie-den-app-store-f-droid) | [Empfohlene Apps](/nutze/f-droid/apps)

===