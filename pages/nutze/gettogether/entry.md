---
title: GetTogether
icon: icon.png

taxonomy:
    replaces: [meetup]
---
**GetTogether** ist eine [freie](/nutze/freie-software) Event-Management-Seite. Sie ist noch relativ neu, weshalb die meisten Events einen Bezug zu Software-Entwicklung haben. Dafür kannst Du leicht selbst ein "Team" (eine Gruppe) und ein Event zu einem beliebigen Thema erstellen.

! **Webseite:** [GetTogether.community](https://gettogether.community)
===