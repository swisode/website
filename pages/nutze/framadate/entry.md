---
title: Framadate
icon: icon.png

taxonomy:
    replaces: [doodle]
---
**Framadate** ist ein unkomplizierter [freier](/nutze/freie-software) Event- und Umfragedienst im Doodle-Stil. Er kostet nichts, verfolgt Dich nicht und erfordert keine Registrierung. Bereitgestellt wird Framadate von Frankreichs größter Organisation für ethische Software, [Framasoft](https://framasoft.org/en/). 

Falls Dir dieser (oder die vielen weiteren) Dienste von Framasoft gefallen kannst Du optional etwas spenden.

! **Webseite:** [Framadate](https://framadate.org/) | [NixNet.xyz](https://poll.nixnet.xyz/)
===