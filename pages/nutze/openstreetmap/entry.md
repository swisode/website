---
title: OpenStreetMap

icon: icon.svg

taxonomy:
    replaces: [google-maps, apple-maps]
    tag: [f-droid]
---
**OpenStreetMap** ist ein riesiger, von Nutzenden erstellter Kartendienst, den alle bearbeiten und verbessern können. Es ist sozusagen die Wikipedia unter den Karten. Dadurch, dass die Karten von Ortsansässigen erstellt werden, ist der Detailgrad oft viel höher als bei kommerziellen Anbietern.  

Das Projekt wird von verschiedensten Universitäten und Organisationen unterstützt und verfolgt Deine Aktivitäten nicht. Ferner kannst Du durch die Android-App [StreetComplete](https://github.com/westnordost/StreetComplete#download) sehr einfach selbst an der Genauigkeit der Karten mitarbeiten.

! **Webseiten:** [OpenStreetMap](https://openstreetmap.org) | [uMap  (individuelle Karten)](https://umap.openstreetmap.fr/en/) | [OpenRouteService (Routenplanung)](https://maps.openrouteservice.org/)
! **Android-Apps:** [OsmAnd](https://osmand.net/) | [Maps.me](https://maps.me/download/)
! **iPhone-Apps:** [OsmAnd](https://osmand.net/) | [Maps.me](https://maps.me/download/)
! **Unterstützen:** [StreetComplete (Fragen vor Ort beantworten)](https://github.com/westnordost/StreetComplete#download)
===