---
title: LimeSurvey
icon: icon.png

taxonomy:
    replaces: [google-forms]
---
**LimeSurvey** ist eine [freie](/nutze/freie-software) Umfrage-Software. Sie kann über LimeSurvey.org verwendet werden, oder Du kannst sie auch auf Deinem eigenen Server installieren (falls vorhanden).

Die selbst betriebene Version ist kostenlos, die von LimeSurvey betriebene kommt in einer kostenlosen und einer Bezahlvariante.

! **Webseite:** [LimeSurvey.org](https://limesurvey.org)
===