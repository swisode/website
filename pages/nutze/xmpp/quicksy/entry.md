---
title: Quicksy

icon: icon.png

taxonomy:
    replaces: [whatsapp, facebook-messenger, skype]
---
**Quicksy** ist eine besonders einsteigerfreundliche Variante von [XMPP](#xmpp). Die Registrierung läuft hier - wie bei WhatsApp - über die Telefonnummer ab. Kontakte aus Deinem Adressbuch, die ebenfalls Quicksy nutzen, werden Dir automatisch vorgeschlagen. Du kannst aber auch beliebige andere XMPP-Nutzer kontaktieren.

! **Download:** [Android](https://play.google.com/store/apps/details?id=im.quicksy.client&noprocess)

===