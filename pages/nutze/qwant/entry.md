---
title: Qwant
icon: icon.png

taxonomy:
    replaces: [google-search]
---
**Qwant** ist eine Suchmaschine mit Sitz in Frankreich, die einen eigenen Suchindex aufbaut. Da dieser noch unvollständig ist, wird er mit Ergebnissen von Microsoft Bing ergänzt. Ebenso wie bei Startpage findet keine Personalisierung der Trefferliste statt. Ferner gibt es eine Lite-Version ohne JavaScript.

! **Webseite:** [Qwant.com](https://qwant.com) | [Qwant Lite](https://lite.qwant.com/)

===