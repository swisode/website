---
title: Wallabag
icon: icon.svg

taxonomy:
    replaces: [pocket]
---
**Wallabag** ist ein datenschutz-freundlicher, [freier](/nutze/freie-software) Online-Service, über den Du Online-Artikel speichern und später lesen kannst.

Du kannst Dich einfach über den unten stehenden Link für wenige Euro pro Jahr registrieren. Falls Du technisch versiert bist kannst Du auch Deinen eigenen Wallabag-Server betreiben. 

! **Webseite:** [Wallabag.org](https://wallabag.org/de)
! **Registrieren:** [Wallabag.it](https://www.wallabag.it/de)
! **Android-Apps:** Wallabag ([Google Play](https://play.google.com/store/apps/details?id=fr.gaulupeau.apps.InThePoche&noprocess) | [F-Droid](https://f-droid.org/de/packages/fr.gaulupeau.apps.InThePoche/))
! **iOS-Apps:** [Wallabag](https://apps.apple.com/app/wallabag-2-official/id1170800946)
===