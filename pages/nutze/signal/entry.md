---
title: Signal

icon: icon.svg

taxonomy:
    replaces: [whatsapp, facebook-messenger, skype]
---
**Signal** ist eine populäre Anwendung für private Chats, Sprach- und Videoanrufe. Sie kommt mit standardmäßig aktivierter Ende-zu-Ende-Verschlüsselung.

Das Projekt wird von der gemeinnützigen Signal-Stiftung entwickelt, welche großen Wert auf Datensparsamkeit und Sicherheit legt. Die Registrierung erfolgt per Handynummer über die Android- oder iOS-App.

! **Entdecken:** [Webseite](https://signal.org/de/)
! **Download:** [Android](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&noprocess) | [iOS](https://itunes.apple.com/us/app/signal-private-messenger/id874139669) | [Desktop](https://signal.org/de/download/)

===