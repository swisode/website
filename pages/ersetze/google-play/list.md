---
title: Ciao Google Play
subtitle: Empfohlene App-Stores
content:
    items: 
        - '@taxonomy.replaces': google-play
hero_classes: hero-tiny title-h1h2
---
Google Play ist sehr hilfreich bei der Suche nach Apps unter Android, jedoch problematisch im Hinblick auf Deine Privatsphäre. Google verfolgt was installiert wird, und unter den Apps befinden sich viele zweifelhafte bzw. gar schädliche Anwendungen.

Deshalb empfehlen wir Dir die Installation von Apps via F-Droid.