---
title: Ciao WhatsApp
subtitle: Empfohlene Chatprogramme
content:
    items: 
        - '@taxonomy.replaces': whatsapp
    order:
        by: default
        custom:
            - signal
            - xmpp
            - quicksy

hero_classes: hero-tiny title-h1h2
---