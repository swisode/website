---
title: Ciao Facebook
subtitle: Empfohlene soziale Netzwerke

content:
    items: 
        - '@taxonomy.replaces': facebook
    order:
        by: default
        custom:
            - friendica
            - mastodon

hero_classes: hero-tiny title-h1h2
---