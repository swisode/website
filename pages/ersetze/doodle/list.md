---
title: Ciao Doodle
subtitle: Empfohlene Abstimmungsdienste

content:
    items: 
        - '@taxonomy.replaces': doodle
    order:
        by: default
        custom:
            - friendica
            - mastodon

hero_classes: hero-tiny title-h1h2
---