---
title: Ciao MeetUp
subtitle: Empfohlene Veranstaltungs-Plattformen

content:
    items: 
        - '@taxonomy.replaces': [meetup]
hero_classes: hero-tiny title-h1h2
---
MeetUp.com ist eine nützliche Seite, doch sie hat wortwörtlich ihren Preis: MeetUp verlangt viel Geld von den Organisationsteams der Events. Diese erhalten im Gegenzug letztendlich nur eine schlichte Gruppenseite. Das ist unfair den Organisierenden gegenüber, welche gezwungen sind die Kosten selbst zu tragen oder an ihre Teilnehmer weiterzureichen.

Es gibt bessere, gerechtere Möglichkeiten - und sie werden besser, je mehr Verwendung sie finden.