---
title: Ciao Google Chrome
subtitle: Empfohlene Webbrowser
content:
    items: 
        - '@taxonomy.replaces': google-chrome
hero_classes: hero-tiny title-h1h2
---