---
title: Ciao Microsoft Office
subtitle: Empfohlene Bürosoftware
content:
    items: 
        - '@taxonomy.replaces': ms-office
    order:
        by: default
        custom:
            - libreoffice
            - thunderbird
            - onlyoffice
hero_classes: hero-tiny title-h1h2
---