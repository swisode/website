---
title: Über uns
subtitle: Kontext zu dieser Seite
body_classes: title-center title-h1h2
---
# switchingsocial.de

An dieser Stelle entsteht eine deutschsprachige Version der populären englischen Webseite [switching.social](https://web.archive.org/web/20190915101437/https://switching.social/).

**SwitchingSocial** möchte Dich beim Wechsel zu [freier](/nutze/freie-software) und offener Software unterstützen. Unser Ziel ist, dass Du proprietären, unfreien Programmen und Plattformen einfach den Rücken zukehren kannst. So möchten wir Deine Privatsphäre, Deine Meinungsfreiheit und Dein Recht auf Selbstbestimmung stärken.

**Erste Artikel** sind bereits begonnen. Für ein wirklich umfassendes Informations-Angebot wird aber noch etwas Zeit ins Land gehen :-)

Bis dahin erfährst Du anbei mehr über [die Ursprünge von SwitchingSocial](https://web.archive.org/web/20190730203805/https://switching.social/about-this-site/). Wenn Du Anregungen zum Thema hast kannst Du sie uns [in unserem GitLab Repository](https://gitlab.com/swisode/website) oder [über dieses Kontaktformuar](https://www.brickup.de/kontakt) zukommen lassen. 